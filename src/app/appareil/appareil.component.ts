import { Component, Input, OnInit } from '@angular/core';
import {AppareilService} from '../services/appareil.service';
import {AuthService} from '../services/auth.service';

@Component({
  selector:     'app-appareil',
  templateUrl:  './appareil.component.html',
  styleUrls: [  './appareil.component.scss']
})

export class AppareilComponent implements OnInit {


  @Input() appareilName: string;
  @Input() appareilStatus: string;
  @Input() indexOfAppareil: number;
  @Input() id: number;

  constructor(private appareilService: AppareilService, private authService: AuthService) {
  }

  ngOnInit() {
  }

  getStatus() { return this.appareilStatus; }

  getColor() {
    if (this.appareilStatus === 'allumé') {
      return 'green';
    } else {
      return 'red';
    }
  }

  onSwitchStart()   { this.appareilService.switchOn(this.indexOfAppareil); }
  onSwitchDisable() { this.appareilService.switchOff(this.indexOfAppareil); }

}
